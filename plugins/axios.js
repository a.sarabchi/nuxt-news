export default function({ $axios }) {
  $axios.onRequest(config => {
    config.headers.common['Authorization'] = process.env.news_api_token;
  });
}